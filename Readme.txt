Go into "test-af" directory
To install dependance : mvn clean install
To run Tests : mvn test
To start the project : mvn spring-boot:run

then go : localhost:8080/api/test-air-france/ => you should see a Hello world message
Use the postman collection to try the differents endpoints

User's attribute :
    name : String,
    Birthday : String format "YYYY-MM-DD"
    Country : String
    Gender : 0 = MALE, 1 = FEMALE,
    PhnoneNumber = String

/*********DATABASE*********/
Access the DB : localhost:8080/api/test-air-france/h2-console/

Saved Settings : Generic H2(Embedded)
Setting Name : Generic H2(Embedded)
Driver Class : org.h2.Driver
JDBC URL : jdbc:h2:mem:testdb
User Name : sa
Password : password