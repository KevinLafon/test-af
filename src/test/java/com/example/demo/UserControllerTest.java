package com.example.demo;

import static org.hamcrest.Matchers.equalTo;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@AutoConfigureMockMvc
public class UserControllerTest {
    
    @Autowired
    private MockMvc mvc;


    @Test
    public void getHello() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/").accept(MediaType.APPLICATION_JSON))
                                                    .andExpect(status().isOk())
                                                    .andExpect(content().string(equalTo("Hello World !!!")));
    }


    @Test
    public void createFullValidUser() throws Exception {

        String jsonUser = "{\"name\": \"Kevin Lafon\",\"birthDay\": \"1991-03-23\",\"country\": \"French\",\"gender\": 0,\"phoneNumber\": \"00.00.00.00.00\"}";

        mvc.perform(MockMvcRequestBuilders.post("/users")
                                            .content(jsonUser)
                                            .contentType(MediaType.APPLICATION_JSON)
                                            .accept(MediaType.APPLICATION_JSON))
                                            .andExpect(status().isOk())
                                            .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists());
    }

    @Test
    public void createMinimumValidUser() throws Exception {

        String jsonUser = "{\"name\": \"Kevin Lafon\",\"birthDay\": \"1991-03-23\",\"country\": \"French\"}";

        mvc.perform(MockMvcRequestBuilders.post("/users")
                                            .content(jsonUser)
                                            .contentType(MediaType.APPLICATION_JSON)
                                            .accept(MediaType.APPLICATION_JSON))
                                            .andExpect(status().isOk())
                                            .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists());
    }

    @Test
    public void createNullNameUser() throws Exception {
        String jsonUser = "{\"birthDay\": \"1991-03-23\",\"country\": \"French\",\"gender\": 0,\"phoneNumber\": \"00.00.00.00.00\"}";

        mvc.perform(MockMvcRequestBuilders.post("/users")
                                            .content(jsonUser)
                                            .contentType(MediaType.APPLICATION_JSON)
                                            .accept(MediaType.APPLICATION_JSON))
                                            .andExpect(status().isBadRequest());

    }

    @Test
    public void createNullBirthDayUser() throws Exception {
        String jsonUser = "{\"name\": \"Kevin Lafon\",\"country\": \"French\",\"gender\": 0,\"phoneNumber\": \"00.00.00.00.00\"}";

        mvc.perform(MockMvcRequestBuilders.post("/users")
                                            .content(jsonUser)
                                            .contentType(MediaType.APPLICATION_JSON)
                                            .accept(MediaType.APPLICATION_JSON))
                                            .andExpect(status().isBadRequest());

    }

    @Test
    public void createNullCountryUser() throws Exception {
        String jsonUser = "{\"name\": \"Kevin Lafon\",\"birthDay\": \"1991-03-23\",\"gender\": 0,\"phoneNumber\": \"00.00.00.00.00\"}";

        mvc.perform(MockMvcRequestBuilders.post("/users")
                                            .content(jsonUser)
                                            .contentType(MediaType.APPLICATION_JSON)
                                            .accept(MediaType.APPLICATION_JSON))
                                            .andExpect(status().isBadRequest());

    }

    @Test
    public void createUnvalidCountryUser() throws Exception {
        String jsonUser = "{\"name\": \"Kevin Lafon\",\"birthDay\": \"1991-03-23\",\"country\": \"Not French\",\"gender\": 0,\"phoneNumber\": \"00.00.00.00.00\"}";

        mvc.perform(MockMvcRequestBuilders.post("/users")
                                            .content(jsonUser)
                                            .contentType(MediaType.APPLICATION_JSON)
                                            .accept(MediaType.APPLICATION_JSON))
                                            .andExpect(status().isBadRequest());

    }

    @Test
    public void createUnvalidAgeUser() throws Exception {
        String jsonUser = "{\"name\": \"Kevin Lafon\",\"birthDay\": \"3001-03-23\",\"country\": \"French\",\"gender\": 0,\"phoneNumber\": \"00.00.00.00.00\"}";

        mvc.perform(MockMvcRequestBuilders.post("/users")
                                            .content(jsonUser)
                                            .contentType(MediaType.APPLICATION_JSON)
                                            .accept(MediaType.APPLICATION_JSON))
                                            .andExpect(status().isBadRequest());

    }

    @Test
    public void createUnvalidGenderUser() throws Exception {
        String jsonUser = "{\"name\": \"Kevin Lafon\",\"birthDay\": \"3001-03-23\",\"country\": \"French\",\"gender\": 2,\"phoneNumber\": \"00.00.00.00.00\"}";

        mvc.perform(MockMvcRequestBuilders.post("/users")
                                            .content(jsonUser)
                                            .contentType(MediaType.APPLICATION_JSON)
                                            .accept(MediaType.APPLICATION_JSON))
                                            .andExpect(status().isBadRequest());

    }

    @Test
    public void getAllUser() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/users")
                                            .accept(MediaType.APPLICATION_JSON))
                                            .andExpect(status().isOk());
    }

    @Test
    public void getOneUser() throws Exception {
        this.createFullValidUser();

        mvc.perform(MockMvcRequestBuilders.get("/users/{id}", 1)
                                            .accept(MediaType.APPLICATION_JSON))
                                            .andExpect(status().isOk())
                                            .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists());
    }

}
