package com.example.demo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.example.demo.Model.Gender;
import com.example.demo.Model.User;
import com.example.demo.Repository.UserRepository;
import com.example.demo.Service.UserService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

    @Mock
    private UserRepository repository;

    @InjectMocks
    private UserService service;

    @Test
    public void getAllUserTest() {
        List<User> list = new ArrayList<User>();
        User user1 = new User((long)1, "Kevin Lafon 1", "1991-03-23", "French", Gender.MALE, "00.00.00.00.00");
        User user2 = new User((long)2, "Kevin Lafon 2", "1991-03-23", "French", Gender.FEMALE, null);
        User user3 = new User((long)3, "Kevin Lafon 3", "1991-03-23", "French", null, null);

        list.add(user1);
        list.add(user2);
        list.add(user3);

        when(this.repository.findAll()).thenReturn(list);

        List<User> userList = this.service.findAll();
        assertEquals(3, userList.size());
        verify(repository, times(1)).findAll();
    }

    @Test
    public void getOneUserTest() {
        Long id = (long) 1;
        User user = new User(id, "Kevin Lafon 1", "1991-03-23", "French", Gender.MALE, "00.00.00.00.00");

        when(this.repository.findById(id)).thenReturn(Optional.of(user));

        User response = this.service.findById(id).get();
        assertEquals(id, response.getId());
        verify(repository, times(1)).findById(id);
    }


    @Test
    public void saveUserTest() {
        User savedUser = new User ((long)1, "Kevin Lafon", "1991-03-23", "French", Gender.MALE, "00.00.00.00.00");
        User user = new User("Kevin Lafon", "1991-03-23", "French", Gender.MALE, "00.00.00.00.00");

        when(this.repository.save(user)).thenReturn(savedUser);

        this.service.save(user);
        assertEquals(Long.valueOf(1) ,savedUser.getId());
        verify(repository, times(1)).save(user);
    }

    @Test
    public void checkUserAgeTest() {
        String validBirthDay = "2004-01-01";
        String unvalidBirthDay = "2022-01-01";

        Boolean response = this.service.isUserAgeLegal(LocalDate.parse(validBirthDay));
        assertTrue(response);

        response = this.service.isUserAgeLegal(LocalDate.parse(unvalidBirthDay));
        assertFalse(response);
    }

    @Test
    public void checkUserAttributeTest() {
        User validUser1 = new User("Kevin Lafon", "1991-03-23", "French", Gender.MALE, "00.00.00.00.00");
        User validUser2 = new User("Kevin Lafon", "1991-03-23", "French", null, null);
        User unvalidUser0 = new User("Kevin Lafon", "1991-03-23", "Not French", null, null);
        User unvalidUser1 = new User(null, "1991-03-23", "French", null, null);
        User unvalidUser2 = new User("Kevin Lafon", "1991-03-23", null, null, null);
        User unvalidUser3 = new User(null, null, "French", null, null);
        User unvalidUser4 = new User(null, null, null, null, null);

        List<String> list = new ArrayList<>();

        list = this.service.userValidation(validUser1);
        assertEquals(0, list.size());

        list = this.service.userValidation(validUser2);
        assertEquals(0, list.size());

        list = this.service.userValidation(unvalidUser0);
        assertEquals(1, list.size());

        list = this.service.userValidation(unvalidUser1);
        assertEquals(1, list.size());

        list = this.service.userValidation(unvalidUser2);
        assertEquals(1, list.size());

        list = this.service.userValidation(unvalidUser3);
        assertEquals(2, list.size());

        list = this.service.userValidation(unvalidUser4);
        assertEquals(3, list.size());
    }
    
}
