package com.example.demo.Model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Enumerated;
import javax.persistence.EnumType;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="TEST_USER")
public class User {
    
    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable=false, length=50)
    private String name;


    @Column(nullable=false)
    @JsonFormat(pattern="yyyy-MM-dd")
    private LocalDate birthDay;

    @Column(nullable=false, length=50)
    private String country;

    @Enumerated(EnumType.STRING)
    @Column(nullable=true)
    private Gender gender;

    @Column(nullable=true, length=15)
    private String phoneNumber;

    /*
        FOR TESTS ONLY
    */
    public User(Long id, String name, String birthDay, String country, Gender gender, String phoneNumber) {
        this.id = id;
        this.name = name;
        this.birthDay = LocalDate.parse(birthDay);
        this.country = country;
        if (gender != null) {
            this.gender = gender;
        }
        
        if (phoneNumber != null) {
            this.phoneNumber = phoneNumber;
        }        
    }

    /*
        FOR TESTS ONLY
    */
    public User(String name, String birthDay, String country, Gender gender, String phoneNumber) {

        this.name = name;
        if(birthDay != null) {
            this.birthDay = LocalDate.parse(birthDay);
        }        
        this.country = country;    
        this.gender = gender;
        this.phoneNumber = phoneNumber;
       
    }
}
