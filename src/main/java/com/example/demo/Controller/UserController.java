package com.example.demo.Controller;

import java.util.List;
import java.util.Optional;

import com.example.demo.Model.User;
import com.example.demo.Service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class UserController {

    @Autowired
    private UserService service;

    @GetMapping("/")
    public String helloWorld() {
        log.info("Hello world");
        return "Hello World !!!";
    }

    /**
     * 
     * @return a list of all users, or an empty one
     */
    @GetMapping("/users")
    public ResponseEntity<List<User>> getAll() {
        log.info("[UserController][getAll()] Call detected");
        List<User> response = this.service.findAll();
        log.info("[UserController][getAll()] response : " + response.toString());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    /**
     * 
     * @param id the id of an user to find
     * @return   the search user, or an error 400
     */
    @GetMapping("/users/{id}")
    public ResponseEntity<?> get(@PathVariable("id") String id) {
        log.info("[UserController][get()] Call detected with param id = " + id);
        Optional<User> user = this.service.findById(Long.parseLong(id));
        if(user.isEmpty()) {
            log.error("[UserController][getAll()] error 400 : user unknown with id " +id);
            return new ResponseEntity<>("user unknown with id : " + id, HttpStatus.BAD_REQUEST);
        }
        log.info("[UserController][getAll()] response : " + user.toString());
        return new ResponseEntity<>(user.get(), HttpStatus.OK);
    }

    /**
     * 
     * @param newUser a user waiting to be save in DB
     * @return        the user after being save or an error 400 if the newUser attribte aren't valid 
     */
    @PostMapping("/users")
    public ResponseEntity<?> post(@RequestBody User newUser) {
        log.info("[UserController][post()] Call detected with param newUser = " + newUser.toString());
        List<String> unvalidAtribute = this.service.userValidation(newUser);
        if(unvalidAtribute.size() > 0) {
            log.error("[UserController][post()] error 400 : Unvalid user's attribute " + unvalidAtribute.toString());
            return new ResponseEntity<String>("Unvalid user's attribute : " + unvalidAtribute.toString(), HttpStatus.BAD_REQUEST );
        }
        User user = this.service.save(newUser);
        log.info("[UserController][post()] response : " + user.toString());
        return new ResponseEntity<>(user, HttpStatus.OK);
    }
}
