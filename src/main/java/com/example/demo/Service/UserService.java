package com.example.demo.Service;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.example.demo.Model.User;
import com.example.demo.Repository.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class UserService {

    @Autowired
    private UserRepository repo;

  
    /**
     * Retrieve a list of all users
     * @return a list of all users, or an empty one
     */
    public List<User> findAll() {
        log.info("[UserService][findAll()] Call detected");
        List<User> userList = this.repo.findAll();
        log.info("[UserService][findAll()] response (length) : " + userList.size());
        return userList;
    }

    /**
     * Retrieve a list of users from there ids
     * @param ids list of id of the users to retrieve
     * @return    a list of user(s), or an empty one
     */
    public List<User> findAllById(Iterable<Long> ids) {
        log.info("[UserService][findAllById()] Call detected with param list<Id> : " + ids.toString());
        List<User> userList = this.findAllById(ids);
        log.info("[UserService][findAllById()] response : " + userList.toString());
        return userList;
    }

    /**
     * Try to return an user save in DB from it's id
     * @param id the unique identifiant of the user to find in DB
     * @return   the user if it exist, or an empty user if not
     */
    public Optional<User> findById(Long id) {
        log.info("[UserService][findById()] Call detected with param id : " + id);
        Optional<User> user = this.repo.findById(id);
        log.info("[UserService][findById()] response, user is present ? " + user.isPresent());
        return user;
    }

    /**
     * Delete the specified user 
     * @param entity the user to delete
     */
    public void delete(User entity) {
        log.info("[UserService][delete()] Call detected with param entity (id) : " + entity.getId());
        this.repo.delete(entity);        
    }

    /**
     * Delete all user from the DB
     */
    public void deleteAll() {
        log.info("[UserService][deleteAll()] Call detected");
        this.repo.deleteAll();        
    }

    /**
     * Delete the specified users
     * @param ids list of unique identifiants of the users to delete
     */
    public void deleteAllById(Iterable<? extends Long> ids) {
        log.info("[UserService][deleteAllById()] Call detected with param list<Id> : " + ids.toString());
        this.repo.deleteAllById(ids);        
    }

    /**
     * Delete the specified user
     * @param id unique identifiant of the user to delete
     */
    public void deleteById(Long id) {
        log.info("[UserService][deleteById()] Call detected with param id : " + id);
        this.repo.deleteById(id);
        
    }

    /**
     * Create or modify a user
     * @param entity contain the user data to save
     * @return       the user save
     */
    public User save(User entity) {
        log.info("[UserService][save()] Call detected with param entity : " + entity.toString());
        User response = this.repo.save(entity);
        log.info("[UserService][save()] response : " + response.toString());

        return response;
    }

    /**
     * userValidation check if the entity receive attribute Name, Birthday and country are not null,
     * it check also the age of the user receive and if the Country is "French".
     * 
     * @param entity  a user waiting to be create
     * @return        an empty list if evething is ok, or a list of string which inform what problem(s) occured
     */
    public List<String> userValidation(User entity) {
        log.info("[UserService][userValidation()] Call detected with param entity : " + entity.toString());
        List<String> unvalidAtributeList = new ArrayList<>();

        if(entity.getName() == null || entity.getName().trim() == "") {
            unvalidAtributeList.add("Name is null");
        }

        if(entity.getBirthDay() == null) {
            unvalidAtributeList.add("Birthday is null");
        }
        else {
            if(!isUserAgeLegal(entity.getBirthDay())) {
                unvalidAtributeList.add("Too young to register");
            }
        }

        if(entity.getCountry() == null) {
            unvalidAtributeList.add("Country is null");
        }
        else {
            if(!entity.getCountry().equals("French")) {
                unvalidAtributeList.add("Country is not allowed");
            }
        }
        log.info("[UserService][userValidation()] response : " + unvalidAtributeList.toString());

        return unvalidAtributeList;
    }

    /**
     * Calcul the age from a birthdate and compare it the a legalAge to see if the user check is older or younger.
     * @param userBirthDay the birthday of the user being check
     * @return             true if the user is older or equal than the legalAge, else false
     */
    public boolean isUserAgeLegal(LocalDate userBirthDay) {
        log.info("[UserService][isUserAgeLegal()] Call detected with param userBirthDay : " + userBirthDay);
        long legalAge = 18;
        LocalDate today = LocalDate.now();
        long years = ChronoUnit.YEARS.between(userBirthDay, today);
        if(years < legalAge) {
            log.warn("[UserService][isUserAgeLegal()] user age illegal");
            return false;
        }
        log.info("[UserService][isUserAgeLegal()] user age legal");
        return true;
    }
    
}
